﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GHS.ViewModels;

namespace GHS.Controllers
{
    public class ChangeController : Controller
    {
        //
        // GET: /Change/

        public ActionResult Index()
        {
            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("Cookie"))
            {
                HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["Cookie"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(ViewChangePasModel model)
        {
            if (model.conpass != model.newpass)
            {
                   ModelState.AddModelError(string.Empty, "The password and confirmation password must match");
                   Response.Write("<script>alert('The password and confirmation password must match')</script>");
            }
            else
            {
             
            
                if (Session["hid"] != null)
                {
                    if (ModelState.IsValid)
                    {
                        if (model.IsValid(model.newpass, model.oldpass, Session["hid"].ToString()))
                        {
                            Response.Write("<script>alert('Password Changed Successful')</script>");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Provided Data is incorrect.");
                        }
                    }
                }

                else if (Session["uid"] != null && Session["uhid"] != null)
                {
                    if (ModelState.IsValid)
                    {
                        if (model.IsValid2(model.newpass, model.oldpass, Session["uid"].ToString()))
                        {
                            Response.Write("<script>alert('Password Changed Successful')</script>");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Provided Data is incorrect.");
                        }
                    }
                }
            }
            
            return View(model);
        }

        public ActionResult go()
        {
            if (Session["change"].ToString() == "user")
            {
                Session["change"] = null;
                return RedirectToAction("Index", "HotelAccount");
            }
            else if (Session["change"].ToString() == "guest")
            {
                Session["change"] = null;
                return RedirectToAction("Index", "Guest");
            }
            else if (Session["change"].ToString() == "global")
            {
                Session["change"] = null;
                return RedirectToAction("Index", "Global");
            }
            return View();
        }
    }
}
