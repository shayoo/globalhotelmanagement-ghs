﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GHS.ViewModels;
using GHS.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace GHS.Controllers
{
    public class EditUserController : Controller
    {
        //
        // GET: /EditUser/

        public ActionResult Index(string ID,FormCollection collection,ViewEditUserModel model)
        {
            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("Cookie"))
            {
                HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["Cookie"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
            ID=Session["id2"].ToString();
            string _sql = "select Usernames,Passwords from tblUsers where UIDs='" + ID + "'";
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

            SqlConnection cn = new SqlConnection(sString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(_sql, cn);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                model.Username = dr["Usernames"].ToString();
                model.Password = dr["Passwords"].ToString();
            }
            else
            {
                dr.Close();
            }
            cmd.Dispose();
            dr.Close();
            cn.Close();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(ViewEditUserModel model, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                if (model.Update(model.Username, model.Password, Session["id2"].ToString()))
                {
                    Session["id2"] = null;
                    if (Session["usersearch"].ToString() != null && Session["usersearch"].ToString() != "")
                    {
                        Session["usersearch"] = null;
                        return RedirectToAction("Index", "usersearch");
                    }
                    else
                    {
                        return RedirectToAction("Index", "userpaging");
                    }
                }

                else
                {

                }


            }
            return View(model);
        }

        public ActionResult user()
        {
            Session["id2"] = null;
            if (Session["usersearch"].ToString() != null && Session["usersearch"].ToString() != "")
            {
                Session["usersearch"] = null;
                return RedirectToAction("Index", "usersearch");
            }
            else
            {
                return RedirectToAction("Index", "userpaging");
            }
        }
    }
}
