﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GHS.ViewModels;
using System.Net.Mail;

namespace GHS.Controllers
{
    public class ForgotController : Controller
    {
        //
        // GET: /Forgot/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]

        public ActionResult Index(ViewForgotPasswordModel model)
        {

            if (ModelState.IsValid)
            {

                if (model.IsValid(model.HotelName, model.SecretPhrase))
                {

                    try
                    {

                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.cloudams.com");

                        mail.From = new MailAddress("shayan@cloudams.com");
                        mail.To.Add(model.Email);
                        mail.Subject = "Password recovery";
                        string str = "http://192.168.4.11:81/Recovery?id=";
                        mail.Body = "open this link:  " + str + model.HID;

                        SmtpServer.Send(mail);
                        

                        return RedirectToAction("Index", "ForSucess");
                    }
                    catch (SmtpFailedRecipientException s)
                    {
                        Response.Write("<script>alert('Sorry your email address is wrong!')</script>");
                    }
                    catch (SmtpException s)
                    {
                        Response.Write("<script>alert('Sorry your email address is wrong!')</script>");
                    }

                    
                }

                else
                {

                    ModelState.AddModelError("", "The hotel name or Secret phrase provided is incorrect.");

                }

            }
            return View(model);
        }

    }
}
