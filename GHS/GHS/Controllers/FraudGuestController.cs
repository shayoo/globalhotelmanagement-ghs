﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GHS.ViewModels;
using GHS.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;


namespace GHS.Controllers
{
    public class FraudGuestController : Controller
    {
        //
        // GET: /FraudGuest/
        
        public ActionResult Index()
        {
            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("Cookie"))
            {
                HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["Cookie"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
            if (Session["hid"] != null)
            {

                string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

                SqlConnection cn = new SqlConnection(sString);
                cn.Open();
                SqlCommand xcmd = new SqlCommand("select distinct HotelName from tblhotels where HID='" + Session["hid"].ToString() + "'", cn);
                SqlDataReader dr = xcmd.ExecuteReader();
                if (dr.Read())
                {
                    Session["temp2"] = dr["HotelName"].ToString();
                }
                else
                {
                    dr.Close();
                }
                xcmd.Dispose();
                dr.Close();
                cn.Close();
                cn.Dispose();
            }

            if (Session["uid"] != null && Session["uhid"] != null)
            {

                string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

                SqlConnection cn = new SqlConnection(sString);
                cn.Open();
                SqlCommand xcmd = new SqlCommand("select distinct HotelName from tblhotels where HID='" + Session["uhid"].ToString() + "'", cn);
                SqlDataReader dr = xcmd.ExecuteReader();
                if (dr.Read())
                {
                    Session["temp2"] = dr["HotelName"].ToString();
                }
                else
                {
                    dr.Close();
                }
                xcmd.Dispose();
                dr.Close();
                cn.Close();
                cn.Dispose();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();

        }

        [HttpPost]
        public ActionResult Index(ViewFraudGuestModel model,FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                if (model.IsValid(Session["id3"].ToString(), Session["FN"].ToString(), Session["LN"].ToString(), Session["AD"].ToString(), Session["CI"].ToString(), Session["ST"].ToString(), Session["ZI"].ToString(), Session["PH"].ToString(), Session["CEL"].ToString(), Session["temp2"].ToString(), model.Note))
                {
                    Response.Write("<script>alert('A Person is marked as Fraud!')</script>");
                    Session["id3"] = null;
                    Session["LN"] = null;
                    Session["FN"] = null;
                    Session["AD"] = null;
                    Session["CI"] = null;
                    Session["ST"] = null;
                    Session["ZI"] = null;
                    Session["PH"] = null;
                    Session["CEL"] = null;
                    Session["temp2"] = null;
                    return RedirectToAction("Index", "guestpaging");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }


            return View(model);

        }
    }
}
