﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GHS.ViewModels;
using GHS.Models;
namespace GHS.Controllers
{
    public class GlobalController : Controller
    {
        //
        // GET: /Global/
        HotelDBEntities1 db = new HotelDBEntities1();
        [HttpGet]
        public ActionResult Index()
        {
            Session["change"] = null;
            Session["change"] = "global";
            if (Session["hid"] != null)
            {


                var viewModel = new ViewGlobalModel
                {
                    label = Session["hid"].ToString(),
                    firstName =new List<string> { "" },
                    LastName = new List<string> { "" },
                    Addresss = new List<string> { "" },
                    City = new List<string> { "" },
                    State = new List<string> { "" },
                    Zip = new List<string> { "" },
                    Phone = new List<string> { "" },
                    Cell = new List<string> { "" },
                    HotelName = new List<string> { "" },
                    Note = new List<string> { "" }



                };

                return View(viewModel);
            }
            else if (Session["uid"] != null && Session["uhid"] != null)
            {
                var viewModel = new ViewGlobalModel
                {

                    label = Session["uid"].ToString(),
                    firstName = new List<string> { "" },
                    LastName = new List<string> { "" },
                    Addresss = new List<string> { "" },
                    City = new List<string> { "" },
                    State = new List<string> { "" },
                    Zip = new List<string> { "" },
                    Phone = new List<string> { "" },
                    Cell = new List<string> { "" },
                    HotelName = new List<string> { "" },
                    Note = new List<string> { "" }
                
                };
                return View(viewModel);

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(ViewGlobalModel model,FormCollection collection)
        {
            if (Session["hid"] != null)
            {

                var dri = from m in db.tblglobals
                          where m.Dri_lic == model.drilic
                          select m.Dri_lic;

                var fname = from m in db.tblglobals
                            where m.Dri_lic == model.drilic
                            select m.Fname;

                var lname = from m in db.tblglobals
                            where m.Dri_lic == model.drilic
                            select m.LName;

                var Address = from m in db.tblglobals
                              where m.Dri_lic == model.drilic
                              select m.Addresss;

                var Cit = from m in db.tblglobals
                          where m.Dri_lic == model.drilic
                          select m.City;
                var sta = from m in db.tblglobals
                          where m.Dri_lic == model.drilic
                          select m.State;
                var zip = from m in db.tblglobals
                          where m.Dri_lic == model.drilic
                          select m.Zip;
                var pho = from m in db.tblglobals
                          where m.Dri_lic == model.drilic
                          select m.Phone;
                var Cell = from m in db.tblglobals
                           where m.Dri_lic == model.drilic
                           select m.Cell;
                var hotelname = from m in db.tblglobals
                                where m.Dri_lic == model.drilic
                                select m.Hotel_Name;
                var note = from m in db.tblglobals
                           where m.Dri_lic == model.drilic
                           select m.Note;
                var viewModel = new ViewGlobalModel
                {

                    label = Session["hid"].ToString(),
                    firstName = fname.ToList(),
                    LastName = lname.ToList(),
                    Addresss = Address.ToList(),
                    City = Cit.ToList(),
                    State = sta.ToList(),
                    Zip = zip.ToList(),
                    Phone = pho.ToList(),
                    Cell = Cell.ToList(),
                    HotelName = hotelname.ToList(),
                    Note = note.ToList()

                };

                return View(viewModel);
            }
            else if (Session["uid"] != null && Session["uhid"] != null)
            {
                var dri = from m in db.tblglobals
                          where m.Dri_lic == model.drilic
                          select m.Dri_lic;

                var fname = from m in db.tblglobals
                            where m.Dri_lic == model.drilic
                            select m.Fname;

                var lname = from m in db.tblglobals
                            where m.Dri_lic == model.drilic
                            select m.LName;

                var Address = from m in db.tblglobals
                              where m.Dri_lic == model.drilic
                              select m.Addresss;

                var Cit = from m in db.tblglobals
                          where m.Dri_lic == model.drilic
                          select m.City;
                var sta = from m in db.tblglobals
                          where m.Dri_lic == model.drilic
                          select m.State;
                var zip = from m in db.tblglobals
                          where m.Dri_lic == model.drilic
                          select m.Zip;
                var pho = from m in db.tblglobals
                          where m.Dri_lic == model.drilic
                          select m.Phone;
                var Cell = from m in db.tblglobals
                           where m.Dri_lic == model.drilic
                           select m.Cell;
                var hotelname = from m in db.tblglobals
                                where m.Dri_lic == model.drilic
                                select m.Hotel_Name;
                var note = from m in db.tblglobals
                           where m.Dri_lic == model.drilic
                           select m.Note;
                var viewModel = new ViewGlobalModel
                {

                    label = Session["uid"].ToString(),
                    firstName = fname.ToList(),
                    LastName = lname.ToList(),
                    Addresss = Address.ToList(),
                    City = Cit.ToList(),
                    State = sta.ToList(),
                    Zip = zip.ToList(),
                    Phone = pho.ToList(),
                    Cell = Cell.ToList(),
                    HotelName = hotelname.ToList(),
                    Note = note.ToList()

                };
                return View(viewModel);

            }
            return PartialView(model);
        }

        public ActionResult logout()
        {
            Session.Abandon();
            Session.Clear();

            return RedirectToAction("Index", "Home");

        }

    }
}
