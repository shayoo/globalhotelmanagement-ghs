﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GHS.ViewModels;
using GHS.Models;
namespace GHS.Controllers
{
    public class GuestController : Controller
    {
        //
        // GET: /Guest/
        HotelDBEntities1 db = new HotelDBEntities1();
        public ActionResult Index()
        {
            Session["change"] = null;
            Session["change"] = "guest";
            if (Session["hid"] != null)
            {
                string hid = Session["hid"].ToString();
                //var dri = from m in db.tblguests
                //            where m.HID == hid
                //            select m.Dri_lic;

                //var fname = from m in db.tblguests
                //            where m.HID == hid
                //            select m.Fname;

                //var lname = from m in db.tblguests
                //            where m.HID == hid
                //            select m.LName;

                //var Address= from m in db.tblguests
                //            where m.HID == hid
                //            select m.Addresss;

                //var Cit= from m in db.tblguests
                //            where m.HID == hid
                //            select m.City;
                //var sta = from m in db.tblguests
                //          where m.HID == hid
                //          select m.State;
                //var zip = from m in db.tblguests
                //          where m.HID == hid
                //          select m.Zip;
                //var pho = from m in db.tblguests
                //          where m.HID == hid
                //          select m.Phone;
                //var Cell = from m in db.tblguests
                //          where m.HID == hid
                //          select m.Cell;
                //var DOB = from m in db.tblguests
                //          where m.HID == hid
                //          select m.Date_of_Booking;
                //var DOC = from m in db.tblguests
                //          where m.HID == hid
                //          select m.Date_of_Checkout;
                var viewModel = new ViewGuestModel
                {
                    //ldri=dri.ToList(),
                    //lfname=fname.ToList(),
                    //lLname=lname.ToList(),
                    //ladd=Address.ToList(),
                    //lcit=Cit.ToList(),
                    //lstat=sta.ToList(),
                    //lphone=pho.ToList(),
                    //lzip=zip.ToList(),
                    //lcel=Cell.ToList(),
                    //ldob=DOB.ToList(),
                    //ldoc=DOC.ToList(),
                    label = Session["hid"].ToString()

                };

                return View(viewModel);
            }
            if (Session["uid"] != null && Session["uhid"] != null)
            {
                string hid = Session["uhid"].ToString();
                //var dri = from m in db.tblguests
                //          where m.HID == hid
                //          select m.Dri_lic;

                //var fname = from m in db.tblguests
                //            where m.HID == hid
                //            select m.Fname;

                //var lname = from m in db.tblguests
                //            where m.HID == hid
                //            select m.LName;

                //var Address = from m in db.tblguests
                //              where m.HID == hid
                //              select m.Addresss;

                //var Cit = from m in db.tblguests
                //          where m.HID == hid
                //          select m.City;
                //var sta = from m in db.tblguests
                //          where m.HID == hid
                //          select m.State;
                //var zip = from m in db.tblguests
                //          where m.HID == hid
                //          select m.Zip;
                //var pho = from m in db.tblguests
                //          where m.HID == hid
                //          select m.Phone;
                //var Cell = from m in db.tblguests
                //           where m.HID == hid
                //           select m.Cell;
                //var DOB = from m in db.tblguests
                //          where m.HID == hid
                //          select m.Date_of_Booking;
                //var DOC = from m in db.tblguests
                //          where m.HID == hid
                //          select m.Date_of_Checkout;

                var viewModel = new ViewGuestModel
                {
                    //ldri = dri.ToList(),
                    //lfname = fname.ToList(),
                    //lLname = lname.ToList(),
                    //ladd = Address.ToList(),
                    //lcit = Cit.ToList(),
                    //lstat = sta.ToList(),
                    //lphone = pho.ToList(),
                    //lzip = zip.ToList(),
                    //lcel = Cell.ToList(),
                    //ldob = DOB.ToList(),
                    //ldoc = DOC.ToList(),
                    label = Session["uid"].ToString()

                };

                return View(viewModel);

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult logout()
        {
            Session.Abandon();
            Session.Clear();

            return RedirectToAction("Index", "Home");

        }

        //public ActionResult Fraud(string DL,string FN,string LN,string AD,string CI,string ST,string ZI,string PH,string CEL,FormCollection collection)
        //{
        //    Session["id3"] = DL;
        //    Session["FN"] = FN;
        //    Session["LN"] = LN;
        //    Session["AD"] = AD;
        //    Session["CI"] = CI;
        //    Session["ST"] = ST;
        //    Session["ZI"] = ZI;
        //    Session["PH"] = PH;
        //    Session["CEL"] = CEL;
        //    return RedirectToAction("Index", "FraudGuest", new { @class = "fancybox fancybox.iframe" });

        //}


        [HttpPost]
        public ActionResult Index(ViewGuestModel model)
        {
            if (Session["hid"] != null)
            {
                var viewModel = new ViewGuestModel
                {

                    label = Session["hid"].ToString()

                };

             
            }
            if (Session["uid"] != null && Session["uhid"] != null)
            {
                var viewModel = new ViewGuestModel
                {

                    label = Session["uid"].ToString()

                };

             

            }

            if (Session["hid"] != null)
            {
                if (ModelState.IsValid)
                {

                    if (model.IsValid(model.drilic, model.firstName, model.LastName, model.Addresss, model.City, model.State, model.Zip, model.Phone, model.Cell, model.DOB, model.DOC, Session["hid"].ToString()))
                    {

                        return RedirectToAction("Index", "Guest");
                    }

                    else
                    {
                        var viewModel = new ViewGuestModel
                        {

                            label = Session["hid"].ToString()

                        };
                        ModelState.AddModelError("", "Provided Data is incorrect.");

                    }

                }
            }
            //return View(viewModel);
            else if (Session["uid"] != null && Session["uhid"] != null)
            {
                if (ModelState.IsValid)
                {

                    if (model.IsValid(model.drilic, model.firstName, model.LastName, model.Addresss, model.City, model.State, model.Zip, model.Phone, model.Cell, model.DOB, model.DOC, Session["uhid"].ToString()))
                    {

                        return RedirectToAction("Index", "Guest");
                    }

                    else
                    {
                        var viewModel = new ViewGuestModel
                        {

                            label = Session["uid"].ToString()

                        };
                        ModelState.AddModelError("", "Provided Data is incorrect.");

                    }

                }
            }
            return View(model);
        }

    }
}
