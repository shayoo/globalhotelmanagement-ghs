﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GHS.ViewModels;
namespace GHS.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            Session.Abandon();
            Session.Clear();
            return View();
        }

        [HttpPost]
        public ActionResult Index(ViewHomeModel model)
        {

            if (ModelState.IsValid)
            {

                if (model.IsValid(model.HotelName, model.Passwords))
                {

                    System.Web.Security.FormsAuthentication.SetAuthCookie(model.HotelName, model.RememberMe);
                    
                    Session["hid"] = model.HotelName;
                    Session["uid"] = "temp";
                    Session["uhid"] = "temp";
                    
                    return RedirectToAction("Index", "HotelAccount");



                }

                else
                {




                    if (model.IsValid2(model.HotelName, model.Passwords))
                    {

                        System.Web.Security.FormsAuthentication.SetAuthCookie(model.HotelName, model.RememberMe);
                        Session["uid"] = model.HotelName;
                        Session["uhid"] = model.extra;
                    
                        return RedirectToAction("Index", "Guest");



                    }

                    else
                    {

                        ModelState.AddModelError("", "The user name or password provided is incorrect.");

                    }
                }
                
            }
            return View(model);
        }

       
    }
}
