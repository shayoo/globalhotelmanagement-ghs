﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GHS.linqmodel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace GHS.Controllers
{
    public class guestpagingController : Controller
    {
        //
        // GET: /guestpaging/

        public ActionResult Index(SortingPagingData data)
        {
            if (Session["hid"] != null)
            {
                string hid = Session["hid"].ToString();
                gridDataContext db = new gridDataContext();
                IQueryable<tblguest> userlist = from rows in db.tblguests
                                               where rows.HID == hid
                                               select rows;

                data.PageSize = 3;
                if (data.PageSize > 0)
                {
                    data.PageCount = userlist.Count() / data.PageSize;
                    int start = (data.PageSize * data.PageNumber);
                    userlist = userlist.Skip(start).Take(data.PageSize);
                }

                ViewData["userlist"] = userlist;
                ViewData["SortingPagingData"] = data;
            }
            else if (Session["uid"] != null && Session["uhid"] != null)
            {
                string hid = Session["uhid"].ToString();
                gridDataContext db = new gridDataContext();
                IQueryable<tblguest> userlist = from rows in db.tblguests
                                               where rows.HID == hid
                                               select rows;

                data.PageSize = 4;
                if (data.PageSize > 0)
                {
                    data.PageCount = userlist.Count() / data.PageSize;
                    int start = (data.PageSize * data.PageNumber);
                    userlist = userlist.Skip(start).Take(data.PageSize);
                }

                ViewData["userlist"] = userlist;
                ViewData["SortingPagingData"] = data;
            }

            return View();
        }

        public ActionResult Fraud(string DL, string FN, string LN, string AD, string CI, string ST, string ZI, string PH, string CEL, FormCollection collection)
        {
            Session["id3"] = DL;
            Session["FN"] = FN;
            Session["LN"] = LN;
            Session["AD"] = AD;
            Session["CI"] = CI;
            Session["ST"] = ST;
            Session["ZI"] = ZI;
            Session["PH"] = PH;
            Session["CEL"] = CEL;
            return RedirectToAction("Index", "FraudGuest");

        }

    }
}
