﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GHS.linqmodel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace GHS.Controllers
{
    public class userpagingController : Controller
    {
        //
        // GET: /userpaging/

        public ActionResult Index(SortingPagingData data)
        {
            string hid = Session["hid"].ToString();
            gridDataContext db = new gridDataContext();
            IQueryable<tblUser> userlist = from rows in db.tblUsers
                                           where rows.Statuss == "true"
                                           where rows.HID == hid
                                           select rows;
            
            data.PageSize = 4;
            if (data.PageSize > 0)
            {
                data.PageCount = userlist.Count() / data.PageSize;
                int start = (data.PageSize * data.PageNumber);
                userlist = userlist.Skip(start).Take(data.PageSize);
            }

            ViewData["userlist"] = userlist;
            ViewData["SortingPagingData"] = data;

            return View();
        }
        
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            SortingPagingData model = new SortingPagingData();
            TryUpdateModel<SortingPagingData>(model);
            Session["search"] = model.Searchun;
            if (Session["search"].ToString() != null || Session["search"].ToString() != "")
            {
                return RedirectToAction("Index", "usersearch");
            }
            return View();
        }

        public ActionResult Edit(string ID)
        {
            Session["id2"] = ID;
            return RedirectToAction("Index", "EditUser");

        }

        public ActionResult Delete(string ID, FormCollection collection)
        {
            string hid = Session["hid"].ToString();
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

            SqlConnection cn = new SqlConnection(sString);
            cn.Open();

            SqlCommand cmd = new SqlCommand("update tblUsers set Statuss='false' where UIDs='" + ID + "' and HID='" + hid + "'", cn);

            cmd.ExecuteNonQuery();
            cmd.Dispose();
            cn.Close();
            return RedirectToAction("Index", "userpaging");
        }

        public ActionResult Del()
        {
            try
            {
                string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
                string hid = Session["hid"].ToString();
                SqlConnection cn = new SqlConnection(sString);
                cn.Open();
                SqlCommand xcmd = new SqlCommand("update tblUsers set Statuss='false' where HID='"+hid+"'", cn);
                xcmd.ExecuteNonQuery();
                xcmd.Dispose();
                cn.Close();
                return RedirectToAction("Index", "userpaging");
            }
            catch (SqlException)
            {
                Response.Write("<script>alert('There is no data!')</script>");
            }
            return View();

        }
    }
}
