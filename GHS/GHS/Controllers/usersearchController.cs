﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GHS.linqmodel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace GHS.Controllers
{
    public class usersearchController : Controller
    {
        //
        // GET: /usersearch/

        public ActionResult Index(SortingPagingData data)
        {
            string hid = Session["hid"].ToString();
            gridDataContext db = new gridDataContext();
            IQueryable<tblUser> userlist = from rows in db.tblUsers
                                           where rows.Statuss == "true"
                                           where rows.HID == hid
                                           where rows.Usernames==Session["search"].ToString()
                                           select rows;

            data.PageSize = 4;
            if (data.PageSize > 0)
            {
                data.PageCount = userlist.Count() / data.PageSize;
                int start = (data.PageSize * data.PageNumber);
                userlist = userlist.Skip(start).Take(data.PageSize);
            }

            ViewData["userlist"] = userlist;
            ViewData["SortingPagingData"] = data;
            Session["usersearch"] = "hello";
            return View();
        }

        public ActionResult Edit(string ID)
        {
            
            Session["id2"] = ID;
            return RedirectToAction("Index", "EditUser");

        }

        public ActionResult user()
        {

            Session["search"] = null;
            return RedirectToAction("Index", "userpaging");

        }

        public ActionResult Delete(string ID, FormCollection collection)
        {
            string hid = Session["hid"].ToString();
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

            SqlConnection cn = new SqlConnection(sString);
            cn.Open();

            SqlCommand cmd = new SqlCommand("update tblUsers set Statuss='false' where UIDs='" + ID + "' and HID='" + hid + "'", cn);

            cmd.ExecuteNonQuery();
            cmd.Dispose();
            cn.Close();
            return RedirectToAction("Index", "userpaging");
        }

    }
}
