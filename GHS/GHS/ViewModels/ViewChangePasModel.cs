﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace GHS.ViewModels
{
    
    public class ViewChangePasModel
    {
        [Required(ErrorMessage = "Old Password is required")]
        [StringLength(50, ErrorMessage = "Password must be at least 5 characters long.", MinimumLength = 5)]
        public string oldpass { get; set; }

        [Required(ErrorMessage = "New Password is required")]
        [StringLength(50, ErrorMessage = "Password must be at least 5 characters long.", MinimumLength = 5)]
        public string newpass { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(50, ErrorMessage = "Password must be at least 5 characters long.", MinimumLength = 5)]
        //[Compare("newpass", ErrorMessage = "The new password and confirmation password do not match.")]
        public string conpass { get; set; }

        public bool IsValid(string npass, string opass, string id)
        {
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

            SqlConnection cn = new SqlConnection(sString);
            cn.Open();
            SqlCommand xcmd = new SqlCommand("update tblhotels set Passwords='"+npass+"' where Passwords='"+opass+"' and HID='"+id+"'", cn);
            xcmd.ExecuteNonQuery();
            xcmd.Dispose();
            cn.Close();
            cn.Dispose();
            return true;
            
        }

        public bool IsValid2(string npass, string opass, string id)
        {
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

            SqlConnection cn = new SqlConnection(sString);
            cn.Open();
            SqlCommand xcmd = new SqlCommand("update tblUsers set Passwords='" + npass + "' where Passwords='" + opass + "' and UIDs='" + id + "'", cn);
            xcmd.ExecuteNonQuery();
            xcmd.Dispose();
            cn.Close();
            cn.Dispose();
            return true;

        }
    }
}