﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace GHS.ViewModels
{
    public class ViewEditUserModel
    {
        [Required(ErrorMessage = "Username is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Username is not in correct format")]
        public string Username { get; set; }


        [Required(ErrorMessage = "Password is required")]
        [StringLength(50, ErrorMessage = "Password must be at least 5 characters long.", MinimumLength = 5)]
        public string Password { get; set; }

        public bool Update(string _uname, string _pas, string ui)
        {
            
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

            SqlConnection cn = new SqlConnection(sString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("Update tblUsers Set Usernames='" + _uname + "',Passwords='" + _pas + "' Where UIDs='" + ui +"'", cn);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            cn.Close();
            return true;
        }

    }
}