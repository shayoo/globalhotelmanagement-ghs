﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace GHS.ViewModels
{
    public class ViewForgotPasswordModel
    {
        

        [Required(ErrorMessage = "Hotel ID is required")]
        public string HotelName { get; set; }

        public string HID { get; set; }
        public string Passwords { get; set; }
        public string Email { get; set; }

        [Required(ErrorMessage = "Secret Phrase or word is required")]
        [StringLength(100, ErrorMessage = "The Secret Phrase or word must be at least 5 characters long.",MinimumLength=5)]
        public string SecretPhrase { get; set; }

        

        public bool IsValid(string _username, string _sp)
        {

            string _sql = "select HID,Passwords,Email from tblhotels where HID='" + _username + "' and SecretPhrase='" + _sp + "'";
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

            SqlConnection cn = new SqlConnection(sString);

            cn.Open();

            SqlCommand cmd = new SqlCommand(_sql, cn);

            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                HID = dr.GetValue(0).ToString();
                Passwords = dr.GetValue(1).ToString();
                Email = dr.GetValue(2).ToString();
                dr.Close();
                dr.Dispose();
                return true;
            }
            else

                return false;



        }
    }
}