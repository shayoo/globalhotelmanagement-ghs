﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace GHS.ViewModels
{
    public class ViewGlobalModel
    {
        public string label { get; set; }

        [Required(ErrorMessage = "Driver License is required")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Driver License is not in correct format")]
        public string drilic { get; set; }


        [Required(ErrorMessage = "First Name is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "First Name is not in correct format")]
        public List<string> firstName { get; set; }

        [Required(ErrorMessage = "Hotel Name is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Hotel Name is not in correct format")]
        public List<string> HotelName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Last Name is not in correct format")]
        public List<string> LastName { get; set; }

        [Required(ErrorMessage = "Guest Address is required")]
        public List<string> Addresss { get; set; }

        [Required(ErrorMessage = "State Name is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "State Name is not in correct format")]
        public List<string> State { get; set; }

        [Required(ErrorMessage = "City Name is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "City Name is not in correct format")]
        public List<string> City { get; set; }

        [Required(ErrorMessage = "Zip Code is required")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Zip Code is not in correct format")]
        public List<string> Zip { get; set; }

        [Required(ErrorMessage = "Phone Number is required")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Phone Number is not in correct format")]
        public List<string> Phone { get; set; }

        [Required(ErrorMessage = "Cell Number is required")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Cell Number is not in correct format")]
        public List<string> Cell { get; set; }

        [Required(ErrorMessage = "Note is required")]
        public List<string> Note { get; set; }
    }
}