﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace GHS.ViewModels
{
    public class ViewGuestModel
    {
        public string label { get; set; }
        public List<string> ldri { get; set; }
        public List<string> lfname { get; set; }
        public List<string> lLname { get; set; }
        public List<string> ladd { get; set; }
        public List<string> lcit { get; set; }
        public List<string> lstat { get; set; }
        public List<string> lzip { get; set; }
        public List<string> lphone { get; set; }
        public List<string> lcel { get; set; }
        public List<string> ldob { get; set; }
        public List<string> ldoc { get; set; }


        [Required(ErrorMessage = "Driver License is required")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Driver License is not in correct format")]
        public string drilic { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "First Name is not in correct format")]
        public string firstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Last Name is not in correct format")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Guest Address is required")]
        public string Addresss { get; set; }

        [Required(ErrorMessage = "State Name is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "State Name is not in correct format")]
        public string State { get; set; }

        [Required(ErrorMessage = "City Name is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "City Name is not in correct format")]
        public string City { get; set; }

        [Required(ErrorMessage = "Zip Code is required")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Zip Code is not in correct format")]
        public string Zip { get; set; }

        [Required(ErrorMessage = "Phone Number is required")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Phone Number is not in correct format")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Cell Number is required")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Cell Number is not in correct format")]
        public string Cell { get; set; }

        [Required(ErrorMessage = "Date of Booking is required")]
        [RegularExpression(@"^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$", ErrorMessage = "Date is not in correct format")]
        public string DOB { get; set; }

        [Required(ErrorMessage = "Date of Checkout is required")]
        [RegularExpression(@"^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$", ErrorMessage = "Date is not in correct format")]
        public string DOC { get; set; }

        public bool IsValid(string dri, string fn, string ln, string add, string Cit, string Sta,string zip,string pho,string cell,string db,string dc,string id)
        {
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

            SqlConnection cn = new SqlConnection(sString);
            cn.Open();
            SqlCommand xcmd = new SqlCommand("insert into tblguest values('" + dri + "','" + ln + "','" + fn + "','" + add + "','" + Cit + "','" + Sta + "','" + zip + "','" + pho + "','" + cell + "','" + db + "','" + dc + "','"+id+"')", cn);
            xcmd.ExecuteNonQuery();
            xcmd.Dispose();
            cn.Close();
            cn.Dispose();


            return true;
        }

            }
}