﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;


namespace GHS.ViewModels
{
    public class ViewHotelAccountModel
    {
        public string ids { get; set; }
        public string label { get; set; }
        public List<string> luid { get; set; }
        public List<string> lun { get; set; }
        public List<string> lpass { get; set; }

        
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Username is not in correct format")]
        public string Searchun { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Username is not in correct format")]
        public string Username { get; set; }

        
        [Required(ErrorMessage = "Password is required")]
        [StringLength(50, ErrorMessage = "Password must be at least 5 characters long.", MinimumLength = 5)]
        public string Password { get; set; }

        public bool IsValid(string HID,string UN,string Pass)
        {
            string _sql = "select * from tblUsers where HID='" + HID + "'";
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

            SqlConnection cn = new SqlConnection(sString);

            cn.Open();
            try
            {
                DataSet dr = new DataSet();
                SqlDataAdapter cmd = new SqlDataAdapter(_sql, cn);
                cmd.Fill(dr);
                int i = dr.Tables[0].Rows.Count;
                if (i != null && i != 0)
                {

                    ids = HID.Substring(0, 1) + HID.Last() + "User" + i;
                    SqlCommand cmd2 = new SqlCommand("insert into tblUsers values('" + HID + "','" + ids + "','" + UN + "','" + Pass + "','true')", cn);
                    cmd2.ExecuteNonQuery();
                    cmd2.Dispose();
                }
                else
                {
                    ids = HID.Substring(0, 1) + HID.Last() + "User";
                    SqlCommand cmd3 = new SqlCommand("insert into tblUsers values('" + HID + "','" + ids + "','" + UN + "','" + Pass + "','true')", cn);
                    cmd3.ExecuteNonQuery();
                    cmd3.Dispose();
                }
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
            }
            catch (IndexOutOfRangeException ie)
            {
                cn.Open();
                ids = HID.Substring(0, 1) + HID.Last() + "User";
                SqlCommand cmd3 = new SqlCommand("insert into tblUsers values('" + HID + "','" + ids + "','" + UN + "','" + Pass + "','true')", cn);
                cmd3.ExecuteNonQuery();
                cmd3.Dispose();
                cn.Close();
            }
            

                return true;
            
        }

        public void select()
        {
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

            SqlConnection cn = new SqlConnection(sString);
            cn.Open();
            SqlCommand xcmd = new SqlCommand("select UIDs,Usernames,Passwords from tblUsers where HID='Paradise Hotel'", cn);
            //xcmd.ExecuteNonQuery();
            SqlDataReader ds = xcmd.ExecuteReader(); ;
            //xcmd.Fill(ds);

            if (ds.Read())
            {
                luid = new List<string> { ds[0].ToString() };
            }
            //lun = new List<string> { ds.Tables[0].Rows[0].ItemArray[1].ToString() };
            //lpass = new List<string> { ds.Tables[0].Rows[0].ItemArray[2].ToString() };

            xcmd.Dispose();
            cn.Close();


            
        }

        //public bool Delete(string id)
        //{
        //    string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

        //    SqlConnection cn = new SqlConnection(sString);
        //    cn.Open();
            
        //    SqlCommand cmd = new SqlCommand("update tblUsers set Statuss='false' where UIDs='" + id+"'", cn);
            
        //    cmd.ExecuteNonQuery();
        //    cmd.Dispose();
        //    cn.Close();
        //    return true;
        //}
    }
}