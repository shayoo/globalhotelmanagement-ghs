﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;




namespace GHS.ViewModels
{
     

    public class viewregistermodel
    {
        public string ids { get; set; }
         
             [Required(ErrorMessage = "Hotel Name is required")]
             [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Hotel Name is not in correct format")]
             public string HotelName { get; set; }

             [Required(ErrorMessage = "Hotel Address is required")]
             public string Addresss { get; set; }

             [Required(ErrorMessage = "State Name is required")]
             [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "State Name is not in correct format")]
             public string State { get; set; }

             [Required(ErrorMessage = "City Name is required")]
             [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "City Name is not in correct format")]
             public string City { get; set; }

             [Required(ErrorMessage = "Zip Code is required")]
             [RegularExpression(@"^\d+$", ErrorMessage = "Zip Code is not in correct format")]
             public string Zip { get; set; }

             [Required(ErrorMessage = "Phone Number is required")]
             [RegularExpression(@"^\d+$", ErrorMessage = "Phone Number is not in correct format")]
             public string Phone { get; set; }

             [RegularExpression(@"^\d+$", ErrorMessage = "Phone Number is not in correct format")]
             public string Phone2 { get; set; }

             [Required(ErrorMessage = "Fax Number is required")]
             [RegularExpression(@"^\d+$", ErrorMessage = "Fax Number is not in correct format")]
             public string fax { get; set; }

             [Required(ErrorMessage = "Email is required")]
             [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Email is not in correct format")]
             public string Email { get; set; }

             [Required(ErrorMessage = "Secret Phrase or word is required")]
             [StringLength(50, ErrorMessage = "Secret phrase or word must be at least 5 characters long.", MinimumLength = 5)]
             public string SecretPhrase { get; set; }

             [Required(ErrorMessage = "Password is required")]
             [StringLength(50, ErrorMessage = "Password must be at least 5 characters long.", MinimumLength = 5)]
             public string Passwords { get; set; }


         
         public bool IsValid(string _pass, string _hn,string _add,string _stat,string _city,string _zip,string _phone,string _phone2,string _fax,string _sec,string _email)
         {

             string _sql = "select * from tblhotels where HotelName='"+_hn+"'";
             string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;

             SqlConnection cn = new SqlConnection(sString);

             cn.Open();
             try
             {
             DataSet dr = new DataSet();
             SqlDataAdapter cmd = new SqlDataAdapter(_sql, cn);
             cmd.Fill(dr);
             int i = dr.Tables[0].Rows.Count;
             if (i != null && i != 0)
             {

                 ids = _hn + i;
                 SqlCommand cmd2 = new SqlCommand("insert into tblhotels values('" + ids + "','" + _pass + "','" + _hn + "','" + _add + "','" + _stat + "','" + _city + "','" + _zip + "','" + _phone + "','" + _phone2 + "','" + _fax + "','" + _sec + "','" + _email + "')", cn);
                 cmd2.ExecuteNonQuery();
                 cmd2.Dispose();
             }
             else
             {
                 ids = _hn;
                 SqlCommand cmd3 = new SqlCommand("insert into tblhotels values('" + ids + "','" + _pass + "','" + _hn + "','" + _add + "','" + _stat + "','" + _city + "','" + _zip + "','" + _phone + "','" + _phone2 + "','" + _fax + "','" + _sec + "','" + _email + "')", cn);
                 cmd3.ExecuteNonQuery();
                 cmd3.Dispose();
             }
             cmd.Dispose();
             cn.Close();
             cn.Dispose();
             }
             catch(IndexOutOfRangeException ie)
             {
                 cn.Open();
                 ids = _hn;
                 SqlCommand cmd3 = new SqlCommand("insert into tblhotels values('" + ids + "','" + _pass + "','" + _hn + "','" + _add + "','" + _stat + "','" + _city + "','" + _zip + "','" + _phone + "','" + _phone2 + "','" + _fax + "','" + _sec + "','" + _email + "')", cn);
                 cmd3.ExecuteNonQuery();
                 cmd3.Dispose();
                 cn.Close();
             }
             
             return true;

         }

//         public class cap
//         {
//                           Private _captchaValue As String
//<Required(ErrorMessage:="*")> _
//<DisplayName("Captcha: please enter the text shown on the picture above")> _
//<ValidateMvcCaptcha(ErrorMessage:="Captcha response is not valid.")> _
//Public Property captchaValue() As String
//Get
//Return _captchaValue
//End Get
//Set(ByVal value As String)
//_captchaValue = value
//End Set
//End Property

//}


    }
}