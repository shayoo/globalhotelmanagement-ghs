﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<GHS.ViewModels.ViewChangePasModel>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Change Password</title>
    <link rel="stylesheet" type="text/css" href="~/Content/css/reset.css" />
<link rel="stylesheet" type="text/css" href="~/Content/css/structure.css" />


</head>
<body>
    <h1 align="center" style="background-color:#09F;">Global Hotel Solution</h1>
    <% using (Html.BeginForm(FormMethod.Post))
       {
           Html.ValidationSummary(true,"Please Correct the errors and try again");
        %>
                <fieldset class="boxBody" style="width:500px; margin-left:30%;margin-top:10%;">
                <h2 align="center">Password Changing</h2><a href="/Change/go" class="rLink" >Back</a>
    <table>
    <tr>
    
    <td><label>Old Password:</label><%: Html.PasswordFor(model => model.oldpass, new { @class = "txtField" })%></td>
    </tr>
    <tr>
    
    
    
    <td><label>New Password:</label><%: Html.PasswordFor(model => model.newpass, new { @class = "txtField" })%></td>
    </tr>
    <tr>
    
    <td><label>Confirm Password:</label><%: Html.PasswordFor(model => model.conpass, new { @class = "txtField" })%></td>
    </tr>
    <tr>
    
    <td>
    <input type="submit" class="btnLogin" value="Submit">
    </td>
    </tr>
    </table>
    </fieldset>
    <% 
       
       } %>
       <footer id="main" style="margin-top:10%;">
  <a href="#">Global Hotel Solution &copy; 2012</a> | <a href="#">by AIT Services</a>
</footer>
</body>
</html>
