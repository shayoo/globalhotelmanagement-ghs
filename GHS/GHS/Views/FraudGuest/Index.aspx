﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<GHS.ViewModels.ViewFraudGuestModel>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Fraud Guest</title>
    <link rel="stylesheet" type="text/css" href="~/Content/css/reset.css" />
<link rel="stylesheet" type="text/css" href="~/Content/css/structure.css" />
</head>
<body style="background-color:White;">
     <% using (Html.BeginForm(FormMethod.Post))
       {
           Html.ValidationSummary(true,"Please Correct the errors and try again");
        %>
                <fieldset class="boxBody" style="width:500px;">
                <a href="/guestpaging" class="rLink" tabindex="6">Back</a>
    <table>
    <tr>
    
    <td><label>Note:</label><%: Html.TextAreaFor(model => model.Note, new { @class = "txtField", style="height:90px;" })%><h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.Note)%></h6></td>
    
    </tr>
    
    <tr>
    
    <td>
    <input type="submit" class="btnLogin" value="Save">
    </td>
    </tr>
    </table>
    </fieldset>
    <% 
       
       } %>
</body>
</html>
