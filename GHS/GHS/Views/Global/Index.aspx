﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GHS.ViewModels.ViewGlobalModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="server">

 <div class="main_content" style="margin-top:2%;">
    
                    <div class="menu">
                    <ul>
                    <% if (Session["uid"].ToString() == "temp" && Session["uhid"].ToString() == "temp")
                           { %><li><%: Html.ActionLink("Manage Users", "Index", "HotelAccount")%></li><% }
                           else
                           {
                               
                           }
                            %>



                    <li><a href="/Guest">Manage Guests</a></li>
                    <li><a class="current" href="#">Global</a></li>
                    <li style="width:100px;">&nbsp</li>
                    <li style="width:100px;">&nbsp</li>
                    <li style="width:60px;">&nbsp</li>
                    <li> Welcome <%: Model.label %></li>
                    <li><a href="/Global/logout">logout</a></li>
                    <li><a href="/Change" class="fancybox fancybox.iframe">Change Password</a></li>

                        

                        
                    </ul>
                    </div> 
                    
                    
        
                    
    <div class="center_content">  
    
    
    
    
    <div class="right_content"> 
    <h2>View Fraud Guests</h2> 
      <% using (Html.BeginForm())
         { %>              
         <table border="6px" style="border-color:Black;">
         <tr>
         <td>
 <label>Enter Driving license:</label><%: Html.TextBoxFor(model => model.drilic, new {@class="txtField" })%>
 <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.drilic)%></h6>
 </td>
 </tr>
 <tr>
 <td>
 <input type="submit" value="GO" class="btnLogin" />
 </td>
 </tr>
 </table>
 <% } %>
     <h2>See</h2>
     
         <div>
          <% using (Html.BeginForm())
             { %>
         
         <table id="rounded-corner" style="width:845px;">
         <thead>
    	<tr>
        	
            
            <th scope="col" class="rounded">First Name</th>
            <th scope="col" class="rounded">Last Name</th>
            <th scope="col" class="rounded">Address</th>
            <th scope="col" class="rounded">City</th>
            <th scope="col" class="rounded">State</th>
            <th scope="col" class="rounded">Zip</th>
            <th scope="col" class="rounded">Phone</th>
            <th scope="col" class="rounded">Cell</th>
            <th scope="col" class="rounded">Hotel Name</th>
            <th scope="col" class="rounded">Note</th>
            
       
        </tr>
    </thead>
     <tbody>
         <tr>
         
         
         <td><% foreach (string fn in Model.firstName)
                {%><%: fn%><br /><% } %></td>
         
         
         
         
         <td><% foreach (string ln in Model.LastName)
                {%><%: ln%><br /><% } %></td>
         
         
         
         
         <td><% foreach (string ad in Model.Addresss)
                {%><%: ad%><br /><% } %></td>
         
         
         
         
         <td><% foreach (string ci in Model.City)
                {%><%: ci%><br /><% } %></td>
         
         
         
         
         <td><% foreach (string st in Model.State)
                {%><%: st%><br /><% } %></td>
         
         
         
         
         <td><% foreach (string zi in Model.Zip)
                {%><%: zi%><br /><% } %></td>
         
         
         
         <td><% foreach (string pn in Model.Phone)
                {%><%: pn%><br /><% } %></td>
         
         
         <td><% foreach (string ce in Model.Cell)
                {%><%: ce%><br /><% } %></td>
         
         
         
         
         <td><% foreach (string hn in Model.HotelName)
                {%><%: hn%><br /><% } %></td>
         
         
         
         
         <td><% foreach (string no in Model.Note)
                {%><%: no%><br /><% } %></td>
         <br />       
         </tr>
         <br />
         </tbody>
         </table>
         
         <% } %>
         </div>  
      </div><!-- end of right content-->
            
                    
  </div>   <!--end of center content -->               
                    
                    
    
    
    <div class="clear"></div>
    </div> <!--end of main content-->



</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
Global
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
</asp:Content>
