﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GHS.ViewModels.ViewGuestModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="server">

     <div class="main_content" style="margin-top:2%;">
    
                    <div class="menu">
                    <ul>
                    <% if (Session["uid"].ToString() == "temp" && Session["uhid"].ToString() == "temp")
                           { %><li><%: Html.ActionLink("Manage Users", "Index", "HotelAccount")%></li><% }
                           else
                           {
                               
                           }
                            %>


                    <li><a class="current" href="#">Manage Guests</a></li>
                    <li><a href="/Global">Global</a></li>
                    <li style="width:100px;">&nbsp</li>
                    <li style="width:100px;">&nbsp</li>
                    <li style="width:60px;">&nbsp</li>
                    <li> Welcome <%: Model.label %></li>
                    <li><a href="/Guest/logout">logout</a></li>
                    <li><a href="/Change" class="fancybox fancybox.iframe">Change Password</a></li>

                        

                        
                    </ul>
                    </div> 
                    
                    
        
                    
    <div class="center_content">  
    
    
    
    
    <div class="right_content"> 
    <h2>Manage Guests</h2> 
                    

<%--<table id="rounded-corner" summary="2007 Major IT Companies' Profit">
    <thead>
    	<tr>
        	<th scope="col" class="rounded-company"></th>
            <th scope="col" class="rounded">Driving License</th>
            <th scope="col" class="rounded">First Name</th>
            <th scope="col" class="rounded">Last Name</th>
            <th scope="col" class="rounded">Address</th>
            <th scope="col" class="rounded">City</th>
            <th scope="col" class="rounded">State</th>
            <th scope="col" class="rounded">Zip</th>
            <th scope="col" class="rounded">Phone</th>
            <th scope="col" class="rounded">Cell</th>
            <th scope="col" class="rounded">Date of Booking</th>
            <th scope="col" class="rounded">Date of  Checkout</th>
            <th scope="col" class="rounded">Mark As Fraud</th>
       
        </tr>
    </thead>
        
    <tbody>
     
    	<tr>
        	<td></td>
            
            <td><% foreach (string dl in Model.ldri)
                   { %>
<%= dl%><br />
<% } %></td>
            
            <td><% foreach (string firname in Model.lfname)
                   { %>
<%= firname%><br />
<% } %></td>
            <td><% foreach (string lasname in Model.lLname)
                   { %>
<%= lasname%><br />
<% } %></td>

<td><% foreach (string add in Model.ladd)
                   { %>
<%= add%><br />
<% } %></td>

<td><% foreach (string city in Model.lcit)
                   { %>
<%= city%><br />
<% } %></td>

<td><% foreach (string state in Model.lstat)
                   { %>
<%= state%><br />
<% } %></td>

<td><% foreach (string zip in Model.lzip)
                   { %>
<%= zip%><br />
<% } %></td>

<td><% foreach (string phone in Model.lphone)
                   { %>
<%= phone%><br />
<% } %></td>

<td><% foreach (string cell in Model.lcel)
                   { %>
<%= cell%><br />
<% } %></td>

<td><% foreach (string DOB in Model.ldob)
                   { %>
<%= DOB%><br />
<% } %></td>

<td><% foreach (string DOC in Model.ldoc)
                   { %>
<%= DOC%><br />
<% } %></td>
            

            <td><% for (int i = 0; i < Model.ldri.Count; i++)
                   { %><%: Html.ActionLink("Fraud", "Fraud", new { DL = Model.ldri[i], FN=Model.lfname[i], LN=Model.lLname[i], AD=Model.ladd[i], CI=Model.lcit[i],ST=Model.lstat[i], ZI=Model.lzip[i], PH=Model.lphone[i],CEL=Model.lcel[i] })%><br /><% } %></td>
            
        </tr>
        
    	    
        
    </tbody>
</table>--%>


	 <iframe id="iFrame1" height="220px" width="800px" runat="server" src="/guestpaging" style="border:1px; margin-top:12px;"></iframe>
     
     <%--<a href="#" class="bt_red"><span class="bt_red_lft"></span><strong>Delete All Guest</strong><span class="bt_red_r"></span></a> --%>
     
     
       <%-- <div class="pagination">
        <span class="disabled"><< prev</span><span class="current">1</span><a href="">2</a><a href="">3</a><a href="">4</a><a href="">5</a>…<a href="">10</a><a href="">11</a><a href="">12</a>...<a href="">100</a><a href="">101</a><a href="">next >></a>
        </div>--%> 
     
     <h2>&nbsp;</h2>
     
      
     <h2>Add Guests</h2>
     
         <div>
         
         <% using (Html.BeginForm(FormMethod.Post))
            { %>
                <fieldset class="boxBody">
                     <table>
                     <tr>
                     

                     <td>
                     <label>Driving License:</label>
                     <%: Html.TextBoxFor(model => model.drilic, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.drilic)%></h6>
                     
                     </td>
                     </tr>

                     <tr>
                     
                     <td>
                     <label>First Name:</label>
                     <%: Html.TextBoxFor(model => model.firstName, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.firstName)%></h6>
                     
                     </td>
                     </tr>
                     
                     <tr>
                     
                     <td>
                     <label>Last Name:</label>
                     <%: Html.TextBoxFor(model => model.LastName, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.LastName)%></h6>
                     
                     </td>
                     </tr>

                     <tr>
                     
                     <td>
                     <label>Guest Address:</label>
                     <%: Html.TextBoxFor(model => model.Addresss, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.Addresss)%></h6>
                     
                     </td>
                     </tr>
                      
                    <tr>
                     
                     <td>
                     <label>City:</label>
                     <%: Html.TextBoxFor(model => model.City, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.City)%></h6>
                     
                     </td>
                     </tr>

                     <tr>
                     
                     <td>
                     <label>State:</label>
                     <%: Html.TextBoxFor(model => model.State, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.State)%></h6>
                     
                     </td>
                     </tr>

                     <tr>
                     
                     <td>
                     <label>Zip:</label>
                     <%: Html.TextBoxFor(model => model.Zip, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.Zip)%></h6>
                     
                     </td>
                     </tr>

                     <tr>
                     
                     <td>
                     <label>Phone:</label>
                     <%: Html.TextBoxFor(model => model.Phone, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.Phone)%></h6>
                     
                     </td>
                     </tr>

                     <tr>
                     
                     <td>
                     <label>Cell:</label>
                     <%: Html.TextBoxFor(model => model.Cell, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.Cell)%></h6>
                     
                     </td>
                     </tr>

                     <tr>
                     
                     <td>
                     <label>Date of Booking:</label>
                     <%: Html.TextBoxFor(model => model.DOB, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.DOB)%></h6>
                     
                     </td>
                     </tr>

                     <tr>
                     
                     <td>
                     <label>Date of Checkout:</label>
                     <%: Html.TextBoxFor(model => model.DOC, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.DOC)%></h6>
                     
                     </td>
                     </tr>


                     <tr>
                     
                     <td>
                     
                     <input type="submit" value="Submit" class="btnLogin" />
                     
                     
                     </td>
                     </tr>
                     </table>
                     
                    
                </fieldset>
                
         <% } %>
         </div>  
      </div><!-- end of right content-->
            
                    
  </div>   <!--end of center content -->               
                    
                    
    
    
    <div class="clear"></div>
    </div> <!--end of main content-->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
Manage Guests
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
</asp:Content>
