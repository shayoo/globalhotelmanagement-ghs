﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<GHS.linqmodel.tblguest>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        GridView1.DataSource = ViewData["userlist"];
        GridView1.DataBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GHS.linqmodel.SortingPagingData data =
   ViewData["SortingPagingData"] as GHS.linqmodel.SortingPagingData;
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells.Clear();
            TableCell pager = new TableCell();
            pager.ColumnSpan = 4;
            pager.HorizontalAlign = HorizontalAlign.Center;
            for (int i = 0; i <= data.PageCount; i++)
            {
                HyperLink lnk = new HyperLink();
                lnk.Text = (i + 1).ToString();
                lnk.NavigateUrl = string.Format("~/guestpaging/index?&pagenumber={0}", i);
                pager.Controls.Add(lnk);
                pager.Controls.Add(new LiteralControl("&nbsp;"));
            }

            e.Row.Cells.Add(pager);
        }
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href="~/Content/style.css" />
</head>
<body>
    <form id="f1" runat="server">
    
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" ForeColor="#333333" GridLines="None" ShowFooter="True" 
             Width="577px" Height="195px" onrowdatabound="GridView1_RowDataBound">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="Driving License">
            <ItemTemplate>
        <asp:Label ID="l1" Text='<%# Bind("Dri_lic") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="First Name">
            <ItemTemplate>
        <asp:Label ID="l2" Text='<%# Bind("FName") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Last Name">
            <ItemTemplate>
        <asp:Label ID="l3" Text='<%# Bind("LName") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address">
            <ItemTemplate>
        <asp:Label ID="l4" Text='<%# Bind("Addresss") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="City">
            <ItemTemplate>
        <asp:Label ID="l5" Text='<%# Bind("City") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="State">
            <ItemTemplate>
        <asp:Label ID="l6" Text='<%# Bind("State") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Zip">
            <ItemTemplate>
        <asp:Label ID="l7" Text='<%# Bind("Zip") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Phone">
            <ItemTemplate>
        <asp:Label ID="l8" Text='<%# Bind("Phone") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cell">
            <ItemTemplate>
        <asp:Label ID="l9" Text='<%# Bind("Cell") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date of Booking">
            <ItemTemplate>
        <asp:Label ID="l10" Text='<%# Bind("Date_of_Booking") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date of Checkout">
            <ItemTemplate>
        <asp:Label ID="l11" Text='<%# Bind("Date_of_Checkout") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:HyperLinkField HeaderText="Mark as Fraud" DataNavigateUrlFields="Dri_lic,FName,LName,Addresss,City,State,Zip,Phone,Cell" DataNavigateUrlFormatString="/guestpaging/Fraud?DL={0}&FN={1}&LN={2}&AD={3}&CI={4}&ST={5}&ZI={6}&PH={7}&CEL={8}" Text="Fraud" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle Font-Bold="True" ForeColor="White" 
            HorizontalAlign="Center" VerticalAlign="Middle" />
        <HeaderStyle BackColor="#60C8F2" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#ECF8FD" HorizontalAlign="Center" VerticalAlign="Middle" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    
    </form>
</body>
</html>
