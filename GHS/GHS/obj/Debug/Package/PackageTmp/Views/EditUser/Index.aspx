﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<GHS.ViewModels.ViewEditUserModel>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Edit User</title>
    <link rel="stylesheet" type="text/css" href="~/Content/css/reset.css" />
<link rel="stylesheet" type="text/css" href="~/Content/css/structure.css" />
</head>
<body style="background-color:White;">
<h6 align="center">Edit User</h6>
     <% using (Html.BeginForm(FormMethod.Post))
       {
           Html.ValidationSummary(true,"Please Correct the errors and try again");
        %>
                <fieldset class="boxBody" style="width:500px; margin-left:50px;">
    <table>
    <tr>
    
    <td><label>Username:<a href="/EditUser/user" class="rLink">Back</a></label><%: Html.TextBoxFor(model => model.Username, new { @class = "txtField" })%></td>
    </tr>
    <tr>
    
    
    
    <td><label>Password:</label><%: Html.PasswordFor(model => model.Password, new { @class = "txtField" })%></td>
    </tr>
    
    <tr>
    
    <td>
    <input type="submit" class="btnLogin" value="Save">
    </td>
    </tr>
    </table>
    </fieldset>
    <% 
       
       } %>
</body>
</html>
