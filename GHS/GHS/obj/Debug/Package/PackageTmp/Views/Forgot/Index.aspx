﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<GHS.ViewModels.ViewForgotPasswordModel>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
            <title>Forgot your Password?</title>
<meta charset="UTF-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="~/Content/css/reset2.css">
<link rel="stylesheet" type="text/css" href="~/Content/css/structure2.css">
     <script src="~/Scripts/MicrosoftAjax.js" type="text/javascript"></script>

<script src="~/Scripts/MicrosoftMvcValidation.js" type="text/javascript"></script>


</head>
<body>
        
 <h1 align="center" style="background-color:#09F;">Global Hotel Solution</h1>
<!--<br>
<br>
<h6 style="font-size:100px;">GHS</h6> -->
<% Html.EnableClientValidation(); %>
<form class="box login" id="f1" runat="server"  style="height:30%;margin-top:10%;">
<h6 style="color:Red"><%: Html.ValidationSummary("Password Recovery was unsuccessful. Please correct the errors and try again.")%></h6>
<br /><br />

	<fieldset class="boxBody">
    <legend style="font-size:larger; color:#69F; border:0px;">Forgot your password?</legend>
    <table>
    <tr>
    <td><label>Hotel ID:</label></td>
    <td><div>
	  <%:Html.TextBoxFor(model => model.HotelName)  %>
      <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.HotelName) %></h6>
      
      </div></td>
      </tr>
	  
      <tr>
    <td><label>Enter Your Secret Phrase or word:</label></td>
    <td><div>
	  <%:Html.PasswordFor(model => model.SecretPhrase)  %>
      
      <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.SecretPhrase)%></h6>
      </div></td>
      </tr>
        
	<tr>
    <td></td>
    <td><p><input type="submit" value="Recover Password" class="btnLogin" /></p></td>
      </tr>
	
    </table>
    </fieldset>
</form>
<footer id="main" style="margin-top:44%">
  <a href="#">Global Hotel Solution &copy; 2012</a> | <a href="#">by AIT Services</a>
</footer>
</body>
</html>
