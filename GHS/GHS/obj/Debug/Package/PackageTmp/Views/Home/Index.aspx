﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<GHS.ViewModels.ViewHomeModel>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title>Login</title>
<meta charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="~/Content/css/reset.css" />
<link rel="stylesheet" type="text/css" href="~/Content/css/structure.css" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
     <h1 align="center" style="background-color:#09F;">Global Hotel Solution</h1>
<!--<br>
<br>
<h6 style="font-size:100px;">GHS</h6> -->

<form class="box login" id="f2" runat="server">
<label>Global Hotel Solution</label>
	<fieldset class="boxBody">
    <label><a href="/Register" class="rLink" tabindex="6">Register</a>Login ID</label>

	  <%:Html.TextBoxFor(model => model.HotelName)  %>
      <h6 style="color:Red"><%: Html.ValidationMessageFor(model=>model.HotelName)%></h6>
	  <label><a href="/Forgot" class="rLink" tabindex="5">Forget your password?</a>Password</label>
	  <%:Html.PasswordFor(model => model.Passwords)  %>
      <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.Passwords)%></h6>
	</fieldset>
	<footer>
	  <label><%: Html.CheckBoxFor(model=>model.RememberMe) %>Remember Me</label>
	  <input type="submit" class="btnLogin" value="Login" tabindex="4">
	</footer>
</form>
<footer id="main" style="margin-top:44%">
  <a href="#">Global Hotel Solution &copy; 2012</a> | <a href="#">by AIT Services</a>
</footer>
</body>
</html>
