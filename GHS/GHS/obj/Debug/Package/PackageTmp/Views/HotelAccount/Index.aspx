﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GHS.ViewModels.ViewHotelAccountModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="server">

    <div class="main_content" style="margin-top:2%;">
    
                    <div class="menu">
                    <ul>
                    <li><% if (Session["hid"].ToString() == null)
                           {
                           }%>
                           <%else{%> 
                           <%: Html.ActionLink("Manage Users", "Index", "HotelAccount", new {@class="current" })%>
                           <%}%></li>

                    <li><a href="/Guest">Manage Guests</a></li>
                    <li><a href="/Global">Global</a></li>
                    <li style="width:100px;">&nbsp</li>
                    <li style="width:100px;">&nbsp</li>
                    <li style="width:60px;">&nbsp</li>
                    
                    <li> Welcome <%: Model.label %></li>
                    <li><a href="/HotelAccount/logout">logout</a></li>
                    <li><a href="/Change" class="fancybox fancybox.iframe">Change Password</a></li>
                    

                        

                        
                    </ul>
                    </div> 
                    
                    
        
                    
    <div class="center_content">  
    
    
    
    
    <div class="right_content"> 
    <h2>Manage Users</h2> 
          <iframe id="iFrame1" height="300px" width="100%" runat="server" src="/userpaging" style="border:1px; margin-top:12px;"></iframe>          
<%--<% using (Html.BeginForm())
   { %>
<table id="rounded-corner">
    <thead>
    	<tr>
        	<th scope="col" class="rounded-company"></th>
            <th scope="col" class="rounded">User ID</th>
            <th scope="col" class="rounded">Username</th>
            <th scope="col" class="rounded">Password</th>
            
            <th scope="col" class="rounded">Edit</th>
            <th scope="col" class="rounded-q4">Delete</th>
        </tr>
    </thead>
        
    <tbody>
      
    	<tr>
        	
            <td></td>
            <td><% foreach (string hids in Model.luid)
                   { %>
<%= hids%><br />
<% } %></td>
            
            <td><% foreach (string uns in Model.lun)
                   { %>
<%= uns%><br />
<% } %></td>
            <td><% foreach (string up in Model.lpass)
                   { %>
<%= up%><br />
<% } %></td>
            
                <% using (Html.BeginForm("Index", "Edit"))
                   { %>
            <td><% for (int i = 0; i < Model.luid.Count; i++)
                   { %><%: Html.ActionLink("Edit", "Edit", new { ID = Model.luid[i] })%><br /><% } %></td>
                   <% } %>
            <td><% for (int i = 0; i < Model.luid.Count; i++)
                   { %><%: Html.ActionLink("Delete", "Delete", new { ID = Model.luid[i] })%><br /><% } %></td>
                   
        </tr>
        
    	
        
    </tbody>
</table>
<% } %>--%>
       <%-- <% using (Html.BeginForm("sea","HotelAccount"))
           { %>
           <label>Search by Username</label> <%: Html.TextBoxFor(model => model.Searchun,new { @class = "txtField" }) %>
                     <input type="submit" value="GO" class="btnLogin" />


   <% } %>--%>
     
     
     
      <%-- <div class="pagination">
        <span class="disabled"><< prev</span><span class="current">1</span><a href="">2</a><a href="">3</a><a href="">4</a><a href="">5</a>…<a href="">10</a><a href="">11</a><a href="">12</a>...<a href="">100</a><a href="">101</a><a href="">next >></a>
        </div> 
     
     <h2>&nbsp;</h2>--%>

     <h2>Add Users</h2>
     
         <div>
         
         <% using (Html.BeginForm(FormMethod.Post)) { %>
                <fieldset class="boxBody">
                     <table>
                                          
                     <td>
                     <label>Username:</label>
                     <%: Html.TextBoxFor(model => model.Username, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.Username)%></h6>
                     
                     </td>
                     </tr>
                     
                     <tr>
                     
                     <td>
                     <label>Password:</label>
                     <%: Html.PasswordFor(model => model.Password, new { @class = "txtField" })%>
                     <h6 style="color:Red"><%: Html.ValidationMessageFor(model => model.Password)%></h6>
                     
                     </td>
                     </tr>

                     <tr>
                     
                     <td>
                     
                     <input type="submit" value="Submit" class="btnLogin" />
                     
                     
                     </td>
                     </tr>
                     </table>
                     
                    
                </fieldset>
                
         <% } %>
         </div>  
      </div><!-- end of right content-->
            
                    
  </div>   <!--end of center content -->               
                    
                    
    
    
    <div class="clear"></div>
    </div> <!--end of main content-->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
Hotel Accounts
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
</asp:Content>
