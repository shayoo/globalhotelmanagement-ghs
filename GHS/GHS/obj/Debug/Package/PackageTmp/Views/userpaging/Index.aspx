﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<GHS.linqmodel.SortingPagingData>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        GridView1.DataSource = ViewData["userlist"];
        GridView1.DataBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GHS.linqmodel.SortingPagingData data =
   ViewData["SortingPagingData"] as GHS.linqmodel.SortingPagingData;
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells.Clear();
            TableCell pager = new TableCell();
            pager.ColumnSpan = 4;
            pager.HorizontalAlign = HorizontalAlign.Center;
            for (int i = 0; i <= data.PageCount; i++)
            {
                HyperLink lnk = new HyperLink();
                lnk.Text = (i + 1).ToString();
                lnk.NavigateUrl = string.Format("~/userpaging/index?&pagenumber={0}", i);
                pager.Controls.Add(lnk);
                pager.Controls.Add(new LiteralControl("&nbsp;"));
            }

            e.Row.Cells.Add(pager);
        }
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href="~/Content/style.css" />
    <link rel="stylesheet" type="text/css" href="~/Content/css/reset.css" />
<link rel="stylesheet" type="text/css" href="~/Content/css/structure.css" />
</head>
<body style="background-color:White;">
<% using (Html.BeginForm())
       { %>
       <table>
       <tr>
       <td>
       <%: Html.TextBoxFor(model => model.Searchun, new {@class="txtField" })%>
       
       </td>
       <td>
       <input type="submit" value="GO" class="btnLogin" />
       </td>
       </tr>
       </table>
   <% } %>
    <form id="f1" runat="server">
    
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" ForeColor="#333333" GridLines="None" ShowFooter="True" 
             Width="577px" Height="195px" onrowdatabound="GridView1_RowDataBound">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="Usernames">
            <ItemTemplate>
        <asp:Label ID="l1" Text='<%# Bind("Usernames") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="User IDs">
            <ItemTemplate>
        <asp:Label ID="l2" Text='<%# Bind("UIDs") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Passwords">
            <ItemTemplate>
        <asp:Label ID="l3" Text='<%# Bind("Passwords") %>' runat="server"></asp:Label>
        </ItemTemplate>
            </asp:TemplateField>
            <asp:HyperLinkField HeaderText="Edit" DataNavigateUrlFields="UIDs"  DataNavigateUrlFormatString="/userpaging/Edit/{0}" Text="Edit" />
            <asp:HyperLinkField HeaderText="Delete" DataNavigateUrlFields="UIDs" DataNavigateUrlFormatString="/userpaging/Delete/{0}"
                Text="Delete" >
            </asp:HyperLinkField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle Font-Bold="True" ForeColor="White" 
            HorizontalAlign="Center" VerticalAlign="Middle" />
        <HeaderStyle BackColor="#60C8F2" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#ECF8FD" HorizontalAlign="Center" VerticalAlign="Middle" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>

    <div align="center" style="width: 154px; height: 49px"><a href="/userpaging/Del" class="bt_red"><span class="bt_red_lft"></span><strong>Delete All Users</strong><span class="bt_red_r"></span></a> </div>
    </form>
</body>
</html>
