﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace Paypal.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Pay()
        {
            //Pay pal process Refer for what are the variable are need to send http://www.paypalobjects.com/IntegrationCenter/ic_std-variable-ref-buy-now.html

            string redirecturl = "";

            //Mention URL to redirect content to paypal site
            redirecturl += "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick&business=" + ConfigurationManager.AppSettings["paypalemail"].ToString();

            //First name i assign static based on login details assign this value
            redirecturl += "&first_name=raza";

            //City i assign static based on login user detail you change this value
            redirecturl += "&city=Maimi";

            //State i assign static based on login user detail you change this value
            redirecturl += "&state=United State";

            //Product Name
            redirecturl += "&item_name=Sugar"; 

            //Product Amount
            redirecturl += "&amount=$50";

            //Business contact id
            //redirecturl += "&business=nravindranmcaatgmail.com";

            //Shipping charges if any
            redirecturl += "&shipping=5";

            //Handling charges if any
            redirecturl += "&handling=5";

            //Tax amount if any
            redirecturl += "&tax=5";

            //Add quatity i added one only statically 
            redirecturl += "&quantity=1";

            //Currency code 
            redirecturl += "¤cy=USD";

            //Success return page url
            redirecturl += "&return=" + ConfigurationManager.AppSettings["SuccessURL"].ToString();

            //Failed return page url
            redirecturl += "&cancel_return=" + ConfigurationManager.AppSettings["FailedURL"].ToString();

            Response.Redirect(redirecturl);

            return View();
        }
    }
}
