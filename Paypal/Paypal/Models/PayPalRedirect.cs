﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PayPalRedirect
{
	public string Url { get; set; }
	public string Token { get; set; }
}
